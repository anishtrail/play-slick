name := """play-slick"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  filters,
  guice,
  "mysql" % "mysql-connector-java" % "5.1.47",
  "com.typesafe.play" %% "play-slick" % "4.0.1",
  "org.flywaydb" % "flyway-core" % "5.2.4"
)


