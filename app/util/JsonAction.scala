package util

import play.api.libs.json.{JsError, JsValue, Json, Reads}
import play.api.libs.typedmap.TypedMap
import play.api.mvc._
import play.api.mvc.Results.BadRequest
import play.api.mvc.request.{RemoteConnection, RequestTarget}

import scala.concurrent.{ExecutionContext, Future}


case class ParsedRequest[T](request: Request[JsValue], parsed: T) extends WrappedRequest[JsValue](request)

case class JsonAction[T] (action: Action[JsValue])(implicit rds: Reads[T]) extends Action[JsValue] {

  override def executionContext: ExecutionContext = action.executionContext
  lazy val parser: BodyParser[JsValue] = action.parser

  def apply(request: Request[JsValue]): Future[Result] = {
    val transformedJson = JsonUtils.requestJsonFormatter(request.body)
    transformedJson.validate[T].map { js =>
      val transformedRequest = new Request[JsValue] {
        override def connection: RemoteConnection = request.connection
        override def method: String = request.method
        override def target: RequestTarget = request.target
        override def version: String = request.version
        override def headers: Headers = request.headers
        override def body: JsValue = transformedJson
        override def attrs: TypedMap = request.attrs
      }
      val wrappedRequest = ParsedRequest(transformedRequest, js)
      action(wrappedRequest)
    } recoverTotal {
      jsError: JsError => Future.successful(BadRequest(Json.toJson(Map("error" -> s"Invalid input json $jsError"))))
    }
  }
}
