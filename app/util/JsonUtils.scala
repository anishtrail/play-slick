package util

import play.api.libs.json.{ JsValue, Json }

object JsonUtils {
  /**
   * Method to format request json. It update values of the passed json where it matches
   * the regex. This is used for adding '-' after '91' for phone number.
   * @param json json which is to be formatted.
   * @return formatted json.
   */
  def requestJsonFormatter(json: JsValue): JsValue = {
    val jsonString = Json.stringify(json)
    val oldFormatPattern = ":\"0(\\d+)\"".r
    val updatedJson = if (oldFormatPattern.findAllIn(jsonString).nonEmpty) {
      jsonString.replaceAll(":\"0(\\d+)\"", ":\"91-$1\"")
    } else {
      jsonString
    }
    Json.parse(updatedJson.replaceAll(":\"91(\\d{10})\"", ":\"91-$1\""))
  }
}
