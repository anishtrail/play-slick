package controllers

import dao.UserDaoTrait
import dto.request.UserRegisterRequestDTO
import javax.inject.Inject
import models.UserModel
import play.api.mvc.{AbstractController, ControllerComponents}
import util.{JsonAction, ParsedRequest}
import scala.concurrent.Future
import dtoFormatter.RequestFormatter.userRegisterRequestDTO
import scala.concurrent.ExecutionContext.Implicits.global

class UserController @Inject()(
                                cc: ControllerComponents,
                                userDao: UserDaoTrait
                              ) extends AbstractController(cc){

  def register: JsonAction[UserRegisterRequestDTO] = JsonAction[UserRegisterRequestDTO] {
    Action.async(parse.json) { rawRequest =>
      val request =  rawRequest.asInstanceOf[ParsedRequest[UserRegisterRequestDTO]].parsed
      val user = UserModel(
        None,
        request.profileName,
        request.phoneNumber,
        request.zipCode,
        request.password
        )
      userDao.addUser(user)
      Future(Ok("user successfully added"))
    }
  }
}