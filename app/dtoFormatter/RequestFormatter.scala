package dtoFormatter

import dto.request.UserRegisterRequestDTO
import play.api.libs.json.Json

object RequestFormatter{
  implicit val userRegisterRequestDTO = Json.format[UserRegisterRequestDTO]
}