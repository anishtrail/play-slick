package common

import com.google.inject.AbstractModule
import javax.inject.{Inject, Singleton}
import org.flywaydb.core.Flyway
import play.api.Configuration

class EagerLoaderModule extends AbstractModule{
  override def configure(): Unit = {
    bind(classOf[StartUpService]).asEagerSingleton()
    bind(classOf[GuiceProdModule]).asEagerSingleton()
  }
}

@Singleton
class StartUpService @Inject()(configuration: Configuration) {

  val url: String = configuration.get[String]("slick.dbs.default.db.url")
  val user: String = configuration.get[String]("slick.dbs.default.db.user")
  val password: String = configuration.get[String]("slick.dbs.default.db.password")
  val flyway: Flyway = Flyway.configure.dataSource(url, user, password).load
  flyway.migrate

}