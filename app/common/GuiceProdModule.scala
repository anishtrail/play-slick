package common


import com.google.inject.AbstractModule
import dao._


class GuiceProdModule extends AbstractModule {

  override def configure(): Unit = {
    bindDao
  }

  def bindDao = {
    bind(classOf[UserDaoTrait]).to(classOf[UserDao])
  }

}
