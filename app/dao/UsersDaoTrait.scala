package dao

import dao.common.BaseTable
import models.UserModel
import slick.dbio.Effect.Write
import slick.jdbc.MySQLProfile.api._
import slick.lifted.Tag

import scala.concurrent.Future

class UserTable(tag: Tag) extends BaseTable[UserModel](tag, "users") {

  def profileName = column[String]("profile_name")

  def phoneNumber = column[String]("phone_number")

  def password = column[String]("password")

  def zipCode =  column[String]("zip_code")

  override def * = (_id.?, profileName, phoneNumber, password, zipCode) <> (UserModel.tupled, UserModel.unapply)

}

trait UserDaoTrait {

  val userTable = TableQuery[UserTable]

  def addUser(user: UserModel): Future[Unit]

}
