package dao

import com.google.inject.Singleton
import javax.inject.Inject
import models.UserModel
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import slick.jdbc.MySQLProfile.api._
import scala.concurrent.ExecutionContext.Implicits.global


@Singleton
class UserDao @Inject()(
                          protected val dbConfigProvider: DatabaseConfigProvider
                        ) extends UserDaoTrait with HasDatabaseConfigProvider[JdbcProfile] {

  override def addUser(user: UserModel)= {
    db.run(userTable += user) map { result =>
      println(s"$result row successfully added")
    } recover {
      case ex: Exception => println(ex)
    }
  }

}