package dao.common

import slick.jdbc.PostgresProfile.api._

abstract class BaseTable[T](tag: Tag, name: String) extends Table[T](tag, name) {

  def _id = column[Long]("_id",O.AutoInc)

}