package dto.request

case class UserRegisterRequestDTO(
 phoneNumber: String,
 profileName: String,
 zipCode: String,
 password: String
)