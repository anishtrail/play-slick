package models

case class UserModel(
 _id: Option[Long],
 phoneNumber: String,
 profileName: String,
 zipCode: String,
 password: String
)